package tsc.abzalov.tm.command.system;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

@SuppressWarnings("unused")
public final class SystemHelpCommand extends AbstractCommand {

    public SystemHelpCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "help";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows all available commands.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val commandService = serviceLocator.getCommandService();
        @NotNull val startupCommands = commandService.getStartupCommands();
        @NotNull val allCommands = commandService.getCommands();
        @NotNull val commandsList = (authService.isSessionInactive())
                ? new ArrayList<>(startupCommands)
                : new ArrayList<>(allCommands);

        commandsList.sort((firstCommand, secondCommand) -> {
            val firstCommandType = firstCommand.getCommandType().getTypeName();
            val secondCommandType = secondCommand.getCommandType().getTypeName();
            return firstCommandType.compareTo(secondCommandType);
        });

        @NotNull val helpMessage = formatHelpMessage(commandsList);

        System.out.println("AVAILABLE COMMANDS");
        System.out.print(helpMessage);
    }

    @NotNull
    private String formatHelpMessage(@NotNull final List<AbstractCommand> commands) {
        @NotNull val builder = new StringBuilder();
        @Nullable AbstractCommand currentCommand;
        @Nullable AbstractCommand previousCommand;

        for (var i = 0; i < commands.size(); i++) {
            currentCommand = commands.get(i);
            previousCommand = (i == 0) ? currentCommand : commands.get(i - 1);

            if (allNotNull(currentCommand, previousCommand)) {
                if (i == 0 || !currentCommand.getCommandType().equals(previousCommand.getCommandType())) {
                    builder
                            .append("\n[")
                            .append(currentCommand.getCommandType().getTypeName().toUpperCase())
                            .append("]\n");
                }
                builder.append(currentCommand).append("\n");
            }
        }

        return builder.append("\n").toString();
    }

}
