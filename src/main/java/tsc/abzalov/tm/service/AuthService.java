package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.util.HashUtil.hash;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IHashingPropertyService propertyService;

    @Nullable
    private User currentUser;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IHashingPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void register(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) {
        if (userService.isUserExist(login, email)) throw new UserAlreadyExistsException(login, email);

        @NotNull val counter = propertyService.getCounterProperty();
        @NotNull val salt = propertyService.getSaltProperty();

        @NotNull val user = new User();
        user.setLogin(login);
        user.setHashedPassword(hash(password, counter, salt));
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);

        currentUser = user;
        userService.create(user);
    }


    @Override
    @SneakyThrows
    public void login(@NotNull final String login, @NotNull final String password) {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        Optional.of(password).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val user = userService.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserIsNotExistException::new);

        if (user.isLocked()) throw new UserLockedException();
        Optional.ofNullable(user.getHashedPassword()).orElseThrow(IncorrectCredentialsException::new);

        @NotNull val counter = propertyService.getCounterProperty();
        @NotNull val salt = propertyService.getSaltProperty();

        if (user.getHashedPassword().equals(hash(password, counter, salt))) {
            currentUser = user;
            return;
        }

        throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void logoff() {
        Optional.ofNullable(currentUser).orElseThrow(SessionIsInactiveException::new);
        currentUser = null;
    }

    @Override
    public boolean isSessionActive() {
        return currentUser != null;
    }

    @Override
    public boolean isSessionInactive() {
        return currentUser == null;
    }

    @Override
    @NotNull
    @SneakyThrows
    public String getCurrentUserId() {
        Optional.ofNullable(currentUser).orElseThrow(SessionIsInactiveException::new);
        return currentUser.getId();
    }

    @Override
    @NotNull
    @SneakyThrows
    public String getCurrentUserLogin() {
        Optional.ofNullable(currentUser).orElseThrow(SessionIsInactiveException::new);
        Optional.ofNullable(currentUser.getLogin()).orElseThrow(IncorrectCredentialsException::new);
        return currentUser.getLogin();
    }

    @Override
    @NotNull
    @SneakyThrows
    public Role getCurrentUserRole() {
        Optional.ofNullable(currentUser).orElseThrow(SessionIsInactiveException::new);
        return currentUser.getRole();
    }

}
