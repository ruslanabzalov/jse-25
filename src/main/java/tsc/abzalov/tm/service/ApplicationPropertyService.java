package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.exception.system.PropertiesAreNotExistException;

import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static tsc.abzalov.tm.util.LiteralConst.*;

public final class ApplicationPropertyService implements IApplicationPropertyService {

    @NotNull
    private static final String PROPERTIES_FILENAME = "application.properties";

    @NotNull
    private static final Map<String, String> SYS_ENVS = System.getenv();

    @NotNull
    private static final Properties SYS_PROPS = System.getProperties();

    @NotNull
    private final Properties properties = new Properties();

    @Override
    @SneakyThrows
    public void initLocalProperties() {
        @Nullable val appProperties = ClassLoader.getSystemResourceAsStream(PROPERTIES_FILENAME);
        Optional.ofNullable(appProperties).orElseThrow(PropertiesAreNotExistException::new);
        properties.load(appProperties);
    }

    @Override
    @NotNull
    public String getAppVersion() {
        return getStringProperty("TM_APP_VERSION", DEFAULT_APP_VERSION);
    }

    @Override
    @NotNull
    public String getDeveloperName() {
        return getStringProperty("TM_DEVELOPER_NAME", DEFAULT_DEVELOPER_NAME);
    }

    @Override
    @NotNull
    public String getDeveloperEmail() {
        return getStringProperty("TM_DEVELOPER_EMAIL", DEFAULT_DEVELOPER_EMAIL);
    }

    @Override
    @NotNull
    public String getSaltProperty() {
        return getStringProperty("TM_HASHING_SALT", DEFAULT_HASHING_SALT);
    }

    @Override
    @NotNull
    public Integer getCounterProperty() {
        if (SYS_ENVS.containsKey("TM_HASHING_COUNTER"))
            return Integer.parseInt(System.getenv("TM_HASHING_COUNTER"));

        if (SYS_PROPS.containsKey("TM_HASHING_COUNTER"))
            return Integer.parseInt(System.getProperty("TM_HASHING_COUNTER", DEFAULT_HASHING_COUNTER));

        return Integer.parseInt(properties.getProperty("TM_HASHING_COUNTER", DEFAULT_HASHING_COUNTER));
    }

    @NotNull
    private String getStringProperty(@NotNull final String key, @NotNull final String defaultValue) {
        if (SYS_ENVS.containsKey(key)) return System.getenv(key);
        if (SYS_PROPS.containsKey(key)) return System.getProperty(key, defaultValue);
        return properties.getProperty(key, defaultValue);
    }

}
