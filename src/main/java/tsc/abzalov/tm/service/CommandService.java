package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.EmptyServiceLocatorException;
import tsc.abzalov.tm.exception.general.IncorrectCommandException;

import java.util.Collection;
import java.util.Optional;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @SneakyThrows
    public void initCommands(@NotNull final IServiceLocator serviceLocator) {
        Optional.of(serviceLocator).orElseThrow(EmptyServiceLocatorException::new);
        commandRepository.initCommands(serviceLocator);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getStartupCommands() {
        return commandRepository.getStartupCommands();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public Collection<String> getCommandArguments() {
        return commandRepository.getCommandArguments();
    }

    @Override
    @NotNull
    @SneakyThrows
    public AbstractCommand getCommandByName(@NotNull final String name) {
        Optional.of(name).orElseThrow(() -> new IncorrectCommandException(name));

        @Nullable val command = commandRepository.getCommandByName(name);
        return Optional.ofNullable(command).orElseThrow(() -> new IncorrectCommandException(name));
    }

    @Override
    @NotNull
    @SneakyThrows
    public AbstractCommand getArgumentByName(@NotNull final String name) {
        Optional.of(name).orElseThrow(() -> new IncorrectCommandException(name));

        @Nullable val argument = commandRepository.getArgumentByName(name);
        return Optional.ofNullable(argument).orElseThrow(() -> new IncorrectCommandException(name));
    }

}
