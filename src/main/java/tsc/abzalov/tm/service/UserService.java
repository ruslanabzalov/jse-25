package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.EmptyEmailException;
import tsc.abzalov.tm.exception.auth.EmptyFirstNameException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IHashingPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IHashingPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public void create(
            @NotNull final String login, @NotNull final String password,
            @NotNull final Role role, @NotNull final String firstName,
            @Nullable final String lastName, @NotNull final String email
    ) {
        create(createUser(login, password, role, firstName, lastName, email));
    }

    @Override
    public void create(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) {
        create(createUser(login, password, USER, firstName, lastName, email));
    }

    @Override
    public boolean isUserExist(@NotNull final String login, @NotNull final String email) {
        val isExistByLogin = userRepository.findByLogin(login) != null;
        val isExistByEmail = userRepository.findByEmail(email) != null;
        return isExistByLogin || isExistByEmail;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val searchedUser = userRepository.findByLogin(login);
        return Optional.ofNullable(searchedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        Optional.of(email).orElseThrow(EmptyEmailException::new);

        @Nullable val searchedUser = userRepository.findByEmail(email);
        return Optional.ofNullable(searchedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editPasswordById(@NotNull final String id, @NotNull final String newPassword) {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(newPassword).orElseThrow(IncorrectCredentialsException::new);

        @NotNull val counter = propertyService.getCounterProperty();
        @NotNull val salt = propertyService.getSaltProperty();

        @Nullable val editedUser = userRepository.editPassword(id, hash(newPassword, counter, salt));
        return Optional.ofNullable(editedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editUserInfoById(
            @NotNull final String id,
            @NotNull final String firstName,
            @Nullable final String lastName
    ) {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(firstName).orElseThrow(EmptyFirstNameException::new);

        @Nullable val editedUser = userRepository.editUserInfo(id, firstName, lastName);
        return Optional.ofNullable(editedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void deleteByLogin(@NotNull final String login) {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        userRepository.deleteByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUnlockById(@NotNull final String id) {
        Optional.of(id).orElseThrow(EmptyIdException::new);

        @Nullable val lockedUnlockedUser = userRepository.lockUnlockById(id);
        return Optional.ofNullable(lockedUnlockedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUnlockByLogin(@NotNull final String login) {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val lockedUnlockedUser = userRepository.lockUnlockByLogin(login);
        return Optional.ofNullable(lockedUnlockedUser).orElseThrow(EntityNotFoundException::new);
    }

    @SneakyThrows
    private void checkMainInfo(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @NotNull final String email
    ) {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        Optional.of(password).orElseThrow(IncorrectCredentialsException::new);
        Optional.of(firstName).orElseThrow(EmptyFirstNameException::new);
        Optional.of(email).orElseThrow(EmptyEmailException::new);
    }

    @NotNull
    private User createUser(
            @NotNull final String login, @NotNull final String password,
            @Nullable final Role role, @NotNull final String firstName,
            @Nullable final String lastName, @NotNull final String email
    ) {
        checkMainInfo(login, password, firstName, email);

        @NotNull val counter = propertyService.getCounterProperty();
        @NotNull val salt = propertyService.getSaltProperty();

        @NotNull val user = new User();
        user.setLogin(login);
        user.setHashedPassword(hash(password, counter, salt));
        if (role != null) user.setRole(role);
        else user.setRole(USER);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        return user;
    }

}
