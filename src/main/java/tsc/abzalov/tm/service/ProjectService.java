package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.model.Project;

public final class ProjectService extends AbstractBusinessEntityService<Project>
        implements IProjectService {

    public ProjectService(@NotNull final IBusinessEntityRepository<Project> repository) {
        super(repository);
    }

}
