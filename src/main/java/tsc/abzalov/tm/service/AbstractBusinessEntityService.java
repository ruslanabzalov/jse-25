package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.BusinessEntity;

import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isIndexIncorrect;

public abstract class AbstractBusinessEntityService<T extends BusinessEntity>
        extends AbstractService<T>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IBusinessEntityRepository<T> repository;

    public AbstractBusinessEntityService(@NotNull final IBusinessEntityRepository<T> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @SneakyThrows
    public long size(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.size(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.isEmpty(userId);
    }

    @Override
    @SneakyThrows
    public int indexOf(@NotNull final String userId, @NotNull final T entity) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(entity).orElseThrow(EmptyEntityException::new);
        return repository.indexOf(userId, entity);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        return repository.findAll(userId);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);

        @Nullable val searchedEntity = repository.findById(userId, id);
        return Optional.ofNullable(searchedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByIndex(@NotNull final String userId, int index) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);

        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        @Nullable val searchedEntity = repository.findByIndex(userId, index);
        return Optional.ofNullable(searchedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByName(@NotNull final String userId, @NotNull final String name) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);

        @Nullable val searchedEntity = repository.findByName(userId, name);
        return Optional.ofNullable(searchedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editById(
            @NotNull final String userId, @NotNull final String id,
            @NotNull final String name, @NotNull String description
    ) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);

        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);

        @Nullable val editedEntity = repository.editById(userId, id, name, description);
        return Optional.ofNullable(editedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByIndex(
            @NotNull final String userId, int index,
            @NotNull final String name, @NotNull String description
    ) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);

        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        Optional.of(name).orElseThrow(EmptyNameException::new);
        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);

        @Nullable val editedEntity = repository.editByIndex(userId, index, name, description);
        return Optional.ofNullable(editedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull String description
    ) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);

        description = Optional.of(description).orElse(DEFAULT_DESCRIPTION);

        @Nullable val editedEntity = repository.editByName(userId, name, description);
        return Optional.ofNullable(editedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        repository.clear(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);
        repository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, int index) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);

        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        repository.removeByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(name).orElseThrow(EmptyNameException::new);
        repository.removeByName(userId, name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T startById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);

        @Nullable val startedEntity = repository.startById(userId, id);
        return Optional.ofNullable(startedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T endById(@NotNull final String userId, @NotNull final String id) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        Optional.of(id).orElseThrow(EmptyIdException::new);

        @Nullable val endedEntity = repository.endById(userId, id);
        return Optional.ofNullable(endedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByName(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @Nullable val firstEntityName = firstEntity.getName();
            @Nullable val secondEntityName = secondEntity.getName();
            if (anyNull(firstEntityName, secondEntityName)) return 0;
            return firstEntityName.compareTo(secondEntityName);
        });

        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStartDate(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @Nullable val firstEntityStartDate = firstEntity.getStartDate();
            @Nullable val secondEntityStartDate = secondEntity.getStartDate();
            if (anyNull(firstEntityStartDate, secondEntityStartDate)) return 0;
            return firstEntityStartDate.compareTo(secondEntityStartDate);
        });

        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByEndDate(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @Nullable val firstEntityEndDate = firstEntity.getEndDate();
            @Nullable val secondEntityEndDate = secondEntity.getEndDate();
            if (anyNull(firstEntityEndDate, secondEntityEndDate)) return 0;
            return firstEntityEndDate.compareTo(secondEntityEndDate);
        });

        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStatus(@NotNull final String userId) {
        Optional.of(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @NotNull val firstEntityStatus = firstEntity.getStatus();
            @NotNull val secondEntityStatus = secondEntity.getStatus();
            return firstEntityStatus.compareTo(secondEntityStatus);
        });

        return entities;
    }

}
