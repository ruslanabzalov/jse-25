package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("First Name is empty! Please, try correct first name.");
    }

}
