package tsc.abzalov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.exception.AbstractException;

public final class PropertyIsNotExist extends AbstractException {

    public PropertyIsNotExist(@NotNull final String key) {
        super("Property with key \"" + key + "\" is not exist.");
    }

}
